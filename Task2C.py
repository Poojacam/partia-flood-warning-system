from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import update_water_levels

def demo2C():

    #Build list of stations
    stations= build_station_list()

    #Update water levels
    update_water_levels(stations)
    
    #print the ten stations with highest relative water level
    print(stations_highest_rel_level(stations,10))
   
    
if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    demo2C()