from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations

def demo1F():
    #Build list of stations
    stations= build_station_list()
    
    #print list of stations with incosistent data
    inconsistent_typical_range_stations(stations)


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    demo1F()
