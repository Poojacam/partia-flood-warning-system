from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels

def demo2B():
    #build station list
    station_list= build_station_list()
   
    #update water levels
    update_water_levels(station_list)
    
    #creates a list of stations over the threshold
    stations_overthreshold= stations_level_over_threshold(station_list,0.8)
    
    #prints station name and relative water level
    for station, relative_level in stations_overthreshold:
        print(station, relative_level)



if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    demo2B()
