from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def demo1E():
    '''Requirements for  Task1E'''

    #Build list of stations
    stations = build_station_list()

    #Print 9 rivers with most stations
    print(rivers_by_station_number(stations, 9))

if __name__ == "__main__":
    print("*** Task1E: CUED Part IA Flood Warning System ***")
    demo1E()
