from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import update_water_levels
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import datetime

def demo2E():
    #Build station list
    stations= build_station_list()

    #update water levels
    update_water_levels(stations)

    #creates list of top 5 stations with highest relative water level
    list_of_stations=stations_highest_rel_level(stations,5)

    #creates list of station names from list of stations
    stationnames= []
    for i in list_of_stations:
        stationnames.append(i[0])
    
    #creates a list of measure_ids of stations in the list of station names
    measureid=[]
    for station in stations:
        if station.name in stationnames:
            measureid.append(station.measure_id)

    dt=10
    
    #creating a list of dates and levels
    for item in measureid:
        dates,levels = fetch_measure_levels(item, dt=datetime.timedelta(days=dt))
        
        #matches the measure_id with the station name
        for station in stations:
            if station.measure_id==item:
                s=station
                break
        #Plots water levels 
        plot_water_levels(s, dates, levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    demo2E()
    
