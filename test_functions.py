from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius, stations_by_river, rivers_by_station_number, rivers_with_station
from haversine import haversine, Unit
import pytest 
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level
from floodsystem.stationdata import update_water_levels

"test for stations_by_distance from geo.py"
def test_call():
    station=stations_within_radius.__call__
    if station== None:
        raise Exception('An error has occured')

"Test that stations have been sorted by distance"
def test_call2():
    stations=build_station_list()
    x=stations_by_distance(stations,(52.2053,0.1218))
    for i in range(len(x)-1):
        if x[i][1]>x[i+1][1]:
            raise Exception('List has not been sorted')

def test_call3():
    stations = build_station_list()
    rivers = rivers_with_station(stations)
    if rivers== None:
        raise Exception('An error has occured')

def test_call4():
    stations=build_station_list()
    rivers = stations_by_river(stations)
    if rivers== None:
        raise Exception('An error has occured')

def test_call5():
    stations=build_station_list()
    N=9
    x = rivers_by_station_number(stations, N)
    if len(x) < N:
        raise Exception('An error has occured, the list is too short')
    if x[0][1] < x[1][1]:
        raise Exception('An error has occured, the list is not sorted')

def test_call7():
    stations=build_station_list()
    tol=0.8
    x= stations_level_over_threshold(stations,tol)
    for i in x: 
        if x[i][1]<x[i+1][1]:
            raise Exception('an error has occured, the list is not sorted')

def test_call8():
    stations= build_station_list()
    update_water_levels(stations)
    N=10
    x= stations_highest_rel_level(stations,N)
    if len(x)<N-1:
        raise Exception('An error has occured, the list is too short')


    

