# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from .utils import sorted_by_key
from haversine import haversine, Unit

def stations_by_distance(stations,p):
    stations_distance=[]
    for station in stations:
        distance= haversine(station.coord,p, unit=Unit.KILOMETERS)
        stations_distance.append((station.name,distance))
    sort_stations_distance = sorted_by_key(stations_distance,1)
    return sort_stations_distance

def stations_within_radius(stations,centre,r):
    stations_in_radius=[]
    for station in stations:
        distance= haversine(station.coord,centre, unit=Unit.KILOMETERS)
        if distance<r:
            stations_in_radius.append(station.name)
    return stations_in_radius