import matplotlib
import numpy as np

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
import matplotlib.pyplot as plt
import datetime
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.analysis import rate_change_water_level
from floodsystem.flood import stations_level_over_threshold

def demo2G():
    stations = build_station_list()
    
    update_water_levels(stations)

    at_risk_stations = stations_level_over_threshold(stations, 1)

    at_risk_station_names = []
    for i in at_risk_stations:
        at_risk_station_names.append(i[0])

    at_risk_station_objects = []
    for station in stations:
        if station.name in at_risk_station_names:
            at_risk_station_objects.append(station)

    
    severe = []
    high = []
    moderate = []
    low = []

    for station in at_risk_station_objects:

        rel_water_level = station.relative_water_level()
        rate = rate_change_water_level(station, p=4)
    

        if rel_water_level != None and rate != None:
            if rel_water_level > 5 and rate > 0:
                severe.append(station.town)
            elif rel_water_level > 5 and rate <= 0:
                high.append(station.town)
            elif rel_water_level <= 5 and rate > 0:
                moderate.append(station.town)
            elif rel_water_level <= 5 and rate <= 0:
                low.append(station.town)

    
    print('Severe', severe)
    print('High', high)
    print('Moderate', moderate)
    print('Low', low)

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    demo2G()





