from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def demo1D():
    '''Requirements for Task 1D'''

    #Build list of stations
    stations = build_station_list()

    #Build set of rivers for set of stations
    riverSet = rivers_with_station(stations)
    riverList = list(riverSet)
    riverList.sort()
    assert riverList[:10] == ['A6 Bridge', 'ALFOLDEAN GS', 'Abbey Heath', 'Abbeymead Avenue', 'Abbeystead Reservoir', 'Abercairn Road', 'Abingdon', 'Abingdon Lock', 'Abingdon Peachcroft', 'Abridge']

    #Build dictionary keys=river values=station names for a set
    riverDict = (stations_by_river(stations))

    #Print the values of stations along rivers
    assert sorted(riverDict.get('River Aire')) == ['Airmyn', 'Apperley Bridge', 'Armley', 'Beal Weir Bridge', 'Bingley', 'Birkin Holme Washlands', 'Carlton Bridge', 'Castleford', 'Chapel Haddlesey', 'Cononley', 'Cottingley Bridge', 'Ferrybridge Lock', 'Fleet Weir', 'Gargrave', 'Kildwick', 'Kirkstall Abbey', 'Knottingley Lock', 'Leeds Crown Point', 'Leeds Crown Point Flood Alleviation Scheme', 'Leeds Knostrop Weir Flood Alleviation Scheme', 'Saltaire', 'Snaygill', 'Stockbridge']
    assert sorted(riverDict.get('River Cam')) == ['Cam', 'Cambridge', 'Cambridge Baits Bite', 'Cambridge Jesus Lock', 'Dernford', 'Great Chesterford', 'Weston Bampfylde']
    assert sorted(riverDict.get('River Thames')) == ['Abingdon Lock', 'Bell Weir', 'Benson Lock', 'Boulters Lock', 'Bray Lock', 'Buscot Lock', 'Caversham Lock', 'Chertsey Lock', 'Cleeve Lock', 'Clifton Lock', 'Cookham Lock', 'Cricklade', 'Culham Lock', 'Days Lock', 'Ewen', 'Eynsham Lock', 'Farmoor', 'Godstow Lock', 'Goring Lock', 'Grafton Lock', 'Hannington Bridge', 'Hurley Lock', 'Iffley Lock', 'Kings Lock', 'Kingston', 'Maidenhead', 'Mapledurham Lock', 'Marlow Lock', 'Marsh Lock', 'Molesey Lock', 'Northmoor Lock', 'Old Windsor Lock', 'Osney Lock', 'Penton Hook', 'Pinkhill Lock', 'Radcot Lock', 'Reading', 'Romney Lock', 'Rushey Lock', 'Sandford-on-Thames', 'Shepperton Lock', 'Shifford Lock', 'Shiplake Lock', 'Somerford Keynes', 'Sonning Lock', 'St Johns Lock', 'Staines', 'Sunbury  Lock', 'Sutton Courtenay', 'Teddington Lock', 'Thames Ditton Island', 'Trowlock Island', 'Walton', 'Whitchurch Lock', 'Windsor Park']

if __name__ == "__main__":
    print("*** Task1D: CUED Part IA Flood Warning System ***")
    demo1D()