from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def demo1C():
    #build list of stations
    stations = build_station_list()
    
    #print stations within 10km radius of Cambridge
    stations_radius=stations_within_radius(stations,(52.2053, 0.1218), 10)
    print(stations_radius)

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    demo1C()
