import matplotlib
import numpy as np

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
import matplotlib.pyplot as plt
import datetime
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit

def demo2F():
    """Requirements for Task2F"""

    stations = build_station_list()

    update_water_levels(stations)

    list_of_high_level_stations = stations_highest_rel_level(stations,5)

    high_level_station_names = []
    for i in list_of_high_level_stations:
        high_level_station_names.append(i[0])

    measureid = []
    for station in stations:
        if station.name in high_level_station_names:
            measureid.append(station.measure_id)

    dt=2
    
    for item in measureid:
        dates,levels = fetch_measure_levels(item, dt=datetime.timedelta(days=dt))

        for station in stations:
            if station.measure_id==item:
                s=station
                break
        
        typicalrange = s.typical_range
        plt.plot((dates[0], dates[-1]),(typicalrange[0], typicalrange[0]), '--')
        plt.plot((dates[0], dates[-1]), (typicalrange[1], typicalrange[1]), '--')
        plot_water_level_with_fit(s, dates, levels, p=4)
        

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    demo2F()

