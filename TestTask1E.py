from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def demo1E():
    '''Requirements for  Task1E'''

    #Build list of stations
    stations = build_station_list()

    #Print 9 rivers with most stations
    assert rivers_by_station_number(stations, 9) == [('River Thames', 55), ('River Avon', 32), ('River Great Ouse', 31), ('River Aire', 23), ('River Calder', 21), ('River Severn', 21), ('River Derwent', 21), ('River Stour', 19), ('River Ouse', 16), ('River Trent', 16)]

if __name__ == "__main__":
    print("*** Task1E: CUED Part IA Flood Warning System ***")
    demo1E()