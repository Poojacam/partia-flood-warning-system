from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def demo1B():
    #Build list of stations
    stations = build_station_list()


    distance_from_Cambridge = stations_by_distance(stations,(52.2053,0.1218))
    #Print 10 closest stations from Cambridge
    print(distance_from_Cambridge[0:10])
    #Print 10 furthest stations from Cambridge 
    print(distance_from_Cambridge[-10:])
   

if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    demo1B()

