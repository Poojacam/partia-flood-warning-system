from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def demo1D():
    '''Requirements for Task 1D'''

    #Build list of stations
    stations = build_station_list()

    #Build set of rivers for set of stations
    riverSet = rivers_with_station(stations)
    riverList = list(riverSet)
    riverList.sort()
    print(riverList[:10])

    #Build dictionary keys=river values=station names for a set
    riverDict = (stations_by_river(stations))

    #Print the values of stations along rivers
    print(sorted(riverDict.get('River Aire')))
    print(sorted(riverDict.get('River Cam')))
    print(sorted(riverDict.get('River Thames')))

if __name__ == "__main__":
    print("*** Task1D: CUED Part IA Flood Warning System ***")
    demo1D()