# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""
from .utils import sorted_by_key
from haversine import haversine, Unit

"This function returns a list of stations sorted by distance away from a co-ordinate p"

def stations_by_distance(stations,p):
    stations_distance=[]
    #Calculate distance from Cambridge
    for station in stations:
        distance= haversine(station.coord,p, unit=Unit.KILOMETERS)
        stations_distance.append((station.name,distance))
    
    #Sort stations by distance 
    sort_stations_distance = sorted_by_key(stations_distance,1)
    return sort_stations_distance

"This function returns the name of stations within a certain radius"
def stations_within_radius(stations,centre,r):
    stations_in_radius=[]
    for station in stations:
        #Calculate distance from Cambridge
        distance= haversine(station.coord,centre, unit=Unit.KILOMETERS)
        
        #Find stations within a radius r from Cambridge
        if distance<r:
            stations_in_radius.append(station.name)
    return stations_in_radius

def rivers_with_station(stations):
    """Build and return set of rivers for set of stations with no repeats"""
    rivers = set()
    for station in stations:
        rivers.add(station.name)
    return rivers

def stations_by_river(stations):
    """Build and return dictionary of river and list of stations for that river"""
    rivers = dict()
    for station in stations:
        if station.river in rivers:
            rivers[station.river].append(station.name)
        else:
            rivers[station.river]=[station.name]
            pass
    return rivers

def rivers_by_station_number(stations, N):
    """Build and return list of tuples with number of stations on river"""

    #Dictionary with rivers and stations
    rivers = stations_by_river(stations)

    #List for rivers and number of stations
    riverNumbers = list()
    for key, value in rivers.items():
        riverNumbers.append(tuple([key, len(value)]))
    
    #Sort list
    riverNumbers.sort(key=lambda x: x[1], reverse=True)

    #List of N rivers to return
    riverNumbersToReturn = riverNumbers[:N]
    del riverNumbers[:N]

    #Add rivers with same number of stations
    while riverNumbers[0][1] == riverNumbersToReturn[-1][1]:
        riverNumbersToReturn.append(riverNumbers[0])
        del riverNumbers[0]
    
    return riverNumbersToReturn

    
    #

