
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.utils import sorted_by_key

#returns a list of tuples of the station names and relative water levels of the stations over the tolerance
def stations_level_over_threshold(stations, tol):
    
    #removing all the inconsistent data from stations
    inconsistent_data= inconsistent_typical_range_stations(stations)
    for item in inconsistent_data:
        stations.remove(item)

    #creating list of tuples of stations over the tolerance
    overthreshold=[]
    for station in stations:
       if station.relative_water_level() != None and station.relative_water_level()>tol :
           overthreshold.append((station.name, station.relative_water_level()))
    
    #sorts the list of tuples in descending order based on the relative water level
    overthreshold_sorted= sorted_by_key(overthreshold, 1, reverse=True)
    return overthreshold_sorted

#returns a list of the top N stations with highest relative water level    
def stations_highest_rel_level(stations, N):
    stations_over_threshold= stations_level_over_threshold(stations,0.8)
    return stations_over_threshold[:N]
    
