# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        def typical_range_consistent(self):
            if self.typical_range==None:
                return False
            if self.typical_range[1]>self.typical_range[0]:
                return True
        
        self.consistent=typical_range_consistent(self)
        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d
    "This method checks the typical high/low range data for consistency"
    
    def relative_water_level(self):
        latest_list= self.latest_level
        typicalrange= self.typical_range
        if latest_list!= None and typicalrange!= None:
            numerator= latest_list- typicalrange[0]
            denominator= typicalrange[1]-typicalrange[0]
            return numerator/denominator
        else:
            return None
"This function returns a list of stations with inconsistent data"        
def inconsistent_typical_range_stations(stations):
    stations_inconsistent=list()
    for station in stations:
        if station.consistent==False:
            stations_inconsistent.append(station)
            
    #sorted_stations_inconsistent=sorted(stations_inconsistent)
    return(stations_inconsistent)
