import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from floodsystem.analysis import polyfit
import matplotlib
import numpy as np

#plots water levels
def plot_water_levels(station, dates, levels):
    #plots dates on x axis and water level on y axis
    plt.plot(dates, levels)
    plt.xlabel('date')
    plt.ylabel('water level(m)')
    #prints the title as the station name
    plt.title(station.name)
    plt.tight_layout()
    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    """Plots both real data and best fit polynomial"""

    plt.plot(dates, levels)
    #Plots real data

    fit = polyfit(dates, levels, p)
    poly = fit[0]
    #Creates polynomial

    x = matplotlib.dates.date2num(dates)
    x1 = np.linspace(x[0], x[-1], 30)
    x2 = x1 - fit[1]
    #Creates polynomial inputs

    plt.plot(x1, poly(x2))
    #Plots polynomial

    plt.xlabel('date')
    plt.ylabel('water level(m)')
    plt.title(station.name)
    plt.tight_layout()
    #Plot is formatted

    plt.show()
