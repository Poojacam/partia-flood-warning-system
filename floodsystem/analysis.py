import matplotlib
import numpy as np
from floodsystem.datafetcher import fetch_measure_levels
import datetime

def polyfit(dates, levels, p):
    """Finds best fit polynomial and date shift"""

    x = matplotlib.dates.date2num(dates)
    #Turns dates into floats

    p_coeff = np.polyfit(x - x[0], levels, p)
    #Calculates polynomial coefficient

    poly = np.poly1d(p_coeff)
    #Creates best fit polynomial

    return poly, x[0]

def rate_change_water_level(station, p):
    """Calculates rate of change of water levels from best fit polynomial"""
    rate = None
    
    try:
        dt = 2
        dates,levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        
        if len(dates) != 0 and len(levels) != 0:

            poly, d0 = polyfit(dates, levels, p)
            gradient = np.polyder(poly)
            time = matplotlib.dates.date2num(dates) - d0
            rate = gradient(time[-1])
    except:
        pass
    
    return rate